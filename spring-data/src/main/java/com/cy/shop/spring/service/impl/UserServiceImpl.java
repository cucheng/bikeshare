package com.cy.shop.spring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.cy.shop.spring.dao.UserDao;
import com.cy.shop.spring.entity.User;
import com.cy.shop.spring.service.UserService;

@Service("userDetailsService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserDetails user = userDao.getUserByUsername(username);
		user.isEnabled();
		user.isAccountNonExpired();
		user.isAccountNonLocked();
		user.isCredentialsNonExpired();
		user.getAuthorities();
		return user;
	}

	@Override
	public List<User> getUsers() {
		// TODO Auto-generated method stub
		return userDao.getUsers();
	}

	@Override
	public User getUserById(long userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getUserByEmail(String email) {
		// TODO Auto-generated method stub
		return null;
	}


}
