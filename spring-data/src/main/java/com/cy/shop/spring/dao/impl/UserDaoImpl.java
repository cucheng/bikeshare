package com.cy.shop.spring.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cy.shop.spring.dao.UserDao;
import com.cy.shop.spring.entity.User;

@Transactional
@Repository
public class UserDaoImpl implements UserDao{

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<User> getUsers() {
		String hql = "from User";
		List<User> users = entityManager.createQuery(hql, User.class).getResultList();
		return users;
	}

	@Override
	public User getUserByUsername(String username) {
		String hql = "FROM User ur WHERE ur.username = ?1";
		TypedQuery<User> query = (TypedQuery<User>) entityManager.createQuery(hql);
		query.setParameter(1, username);
		User user = query.getSingleResult();
		return user;
	}
	
	@Override
	public User getUserById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getUserByEmail(String email) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteUserById(long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteUserByEmail(String email) {
		// TODO Auto-generated method stub
		return false;
	}

}
