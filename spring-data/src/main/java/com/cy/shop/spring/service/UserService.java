package com.cy.shop.spring.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.cy.shop.spring.entity.User;

public interface UserService extends UserDetailsService{

	public List<User> getUsers();
	
	public User getUserById(long userId);
	
	public User getUserByEmail(String email);
}
