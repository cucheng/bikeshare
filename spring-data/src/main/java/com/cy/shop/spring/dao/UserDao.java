package com.cy.shop.spring.dao;

import java.util.List;


import com.cy.shop.spring.entity.User;


public interface UserDao {

	public List<User> getUsers();
	
	public User getUserById(long id);
	
	public User getUserByEmail(String email);
	
	public User getUserByUsername(String username);
	
	public boolean deleteUserById(long id);
	
	public boolean deleteUserByEmail(String email);
	
}
