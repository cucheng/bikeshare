package com.cy.shop.spring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.shop.spring.dao.ShopDao;
import com.cy.shop.spring.entity.Shop;
import com.cy.shop.spring.service.ShopService;

@Service
public class ShopServiceImpl implements ShopService {

	@Autowired
	private ShopDao shopDao;
	@Override
	public List<Shop> getShops() {
		return shopDao.getShops();
	}

	@Override
	public Shop getShopById(long shopId) {
		return shopDao.getShopById(shopId);
	}

	public ShopDao getShopDao() {
		return shopDao;
	}

	public void setShopDao(ShopDao shopDao) {
		this.shopDao = shopDao;
	}

}
