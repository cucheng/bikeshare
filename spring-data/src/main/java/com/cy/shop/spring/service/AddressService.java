package com.cy.shop.spring.service;

import java.util.List;

import com.cy.shop.spring.entity.Address;

public interface AddressService {

	public List<Address> getAddresses();
}
