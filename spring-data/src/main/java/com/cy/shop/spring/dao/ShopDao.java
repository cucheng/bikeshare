package com.cy.shop.spring.dao;

import java.util.List;

import com.cy.shop.spring.entity.Shop;



public interface ShopDao {

	public List<Shop> getShops();
	
	public Shop getShopById(long id);
	
	public boolean deleteShopById(long id);
	
	public Shop createShop(Shop shop);
	
	public Shop updateShop(Shop shop);
}
