package com.cy.shop.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cy.shop.spring.entity.Shop;
import com.cy.shop.spring.service.ShopService;

@Controller
@RequestMapping("shop")
public class ShopController {

	@Autowired
	private ShopService shopService;
	
	private EntityLinks entityLinks;
	
	
	@GetMapping("shops")
	public ResponseEntity<List<Shop>> getShops() {
		System.out.println("------------- Start Service ---------------");
		
		return new ResponseEntity<List<Shop>>(shopService.getShops(), HttpStatus.OK);
	}
}
