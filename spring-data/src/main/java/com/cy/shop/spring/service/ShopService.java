package com.cy.shop.spring.service;

import java.util.List;

import com.cy.shop.spring.entity.Shop;

public interface ShopService {

	public List<Shop> getShops();
	
	public Shop getShopById(long shopId);
}
