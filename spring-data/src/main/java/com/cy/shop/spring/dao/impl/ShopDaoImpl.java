package com.cy.shop.spring.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cy.shop.spring.dao.ShopDao;
import com.cy.shop.spring.entity.Shop;

@Transactional
@Repository
public class ShopDaoImpl implements ShopDao{

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<Shop> getShops() {
		TypedQuery<Shop> query = entityManager.createQuery("from Shop", Shop.class);
		return query.getResultList();
	}

	@Override
	public Shop getShopById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteShopById(long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Shop createShop(Shop shop) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Shop updateShop(Shop shop) {
		// TODO Auto-generated method stub
		return null;
	}

}
