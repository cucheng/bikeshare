package com.cy.shop.client;

import javax.validation.constraints.AssertTrue;

import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.cy.shop.spring.entity.Address;
import com.cy.shop.spring.entity.Shop;

public class SpringDataApplicationTests {

	public void getAllAddressesDemo() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
	    String url = "http://localhost:8080/address/addresses";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<Address[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Address[].class);
        Address[] addresses = responseEntity.getBody();
        for(Address address : addresses) {
              System.out.println("Id:"+address.getId()+", Street:"+address.getStreet()
                      +", Country: "+address.getCountry());
        }
    }
	
	@Test
	public void testUser() {
		System.out.println("Save User Successful!!");
	}
	
	public void getAllShopsDemo() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
	    String url = "http://localhost:8080/shop/shops";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<Shop[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Shop[].class);
        Shop[] shops = responseEntity.getBody();
        for(Shop shop : shops) {
              System.out.println("Id:"+shop.getId()+", Description:"+shop.getDescription()
                      +", Url: "+shop.getUrl());
        }
    }
    public static void main(String args[]) {
    	SpringDataApplicationTests util = new SpringDataApplicationTests();
        //util.getArticleByIdDemo();
    	util.getAllAddressesDemo();
    	//util.getAllShopsDemo();
    	//util.addArticleDemo();
    	//util.updateArticleDemo();
    	//util.deleteArticleDemo();
    } 
}
